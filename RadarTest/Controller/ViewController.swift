//
//  ViewController.swift
//  RadarTest
//
//  Created by dragdimon on 17/02/2020.
//

import UIKit

class ViewController: UIViewController {

    //MARK: Properties
    
    let planeImage = UIImage(named: "plane")
    var radiiOfCircles = [Float]()
    
    //MARK: VC Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawCircles(number: Constants.circlesNumber)
        setRadiiOfCircles()
        drawPlanes(number: Constants.planesNumber)
        
    }
    
    //MARK: Utility Functions
    
    private func setRadiiOfCircles() {
        
        for index in 0..<Constants.circlesNumber {
            let radius = (view.subviews[index].frame.size.height - 2 * Constants.circleLineWidth) / 2
            radiiOfCircles.append(Float(radius))
        }
    }

    private func setCoordinates(radius: Float, center: CGPoint) -> CGPoint {
        // Random angle in [0, 2*pi)
        let theta = Float.random(in: 0..<1) * Float.pi * 2.0
        // Convert Polar to Cartesian
        var x = CGFloat(radius * cos(theta)) + center.x
        var y = CGFloat(radius * sin(theta)) + center.y
        
        // Define quarter of a circle and shift origin of planeImageView
        if x > center.x {
            if y < center.y {
                x -= Constants.planesSize
            } else {
                x -= Constants.planesSize
                y -= Constants.planesSize
            }
        } else if y > center.y {
            y -= Constants.planesSize
        }
        
        return CGPoint(x: x, y: y)
    }
    
    //MARK: Draw Functions
    
    private func drawCircles(number: Int){
        var circleWidth = Constants.circleWidth
        var circleHeight = circleWidth
        
        for _ in 1...number {
            let circleView = CircleView(frame: CGRect(x: view.center.x - circleWidth/2,
                                                      y: view.center.y - circleHeight/2,
                                                      width: circleWidth,
                                                      height: circleHeight))
            view.addSubview(circleView)
            
            // Increase size of next circle
            circleWidth += Constants.deltaCircleWidth
            circleHeight = circleWidth
        }
    }

    private func drawPlanes(number: Int) {
        
        for _ in 1...number {
            
            // Choose random circle and get its radius
            let circleNumber = Int.random(in: 0..<Constants.circlesNumber)
            let radius = radiiOfCircles[circleNumber]
            
            // Generate random point on circle and shift to avoid overlapping
            let originOfPlane = setCoordinates(radius: radius, center: view.center)
            
            // Set planeImageView to defined origin
            let planeImageView = UIImageView(frame: CGRect(x: originOfPlane.x,
                                                           y: originOfPlane.y,
                                                           width: Constants.planesSize,
                                                           height: Constants.planesSize))
            
            planeImageView.image = planeImage
            view.addSubview(planeImageView)
        }
    }
}

