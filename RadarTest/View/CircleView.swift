//
//  CircleView.swift
//  RadarTest
//
//  Created by dragdimon on 17/02/2020.
//

import UIKit

class CircleView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        
        if let context = UIGraphicsGetCurrentContext() {
            
            // Set the circle outerline-width
            context.setLineWidth(Constants.circleLineWidth)
            
            // Set the circle outerline-colour
            UIColor.black.set()
            
            // Create Circle
            let center = CGPoint(x: rect.midX, y: rect.midY)
            let radius = (rect.width - Constants.circleLineWidth) / 2
            context.addArc(center: center, radius: radius, startAngle: 0.0, endAngle: .pi * 2.0, clockwise: true)
                
            // Draw circle
            context.strokePath()
        }
    }

}
