//
//  Constants.swift
//  RadarTest
//
//  Created by dragdimon on 18/02/2020.
//

import UIKit

struct Constants {
    
    static let circlesNumber = 10
    static let planesNumber = 100
    static let circleLineWidth: CGFloat = 1
    static let circleWidth: CGFloat = 40
    static let deltaCircleWidth: CGFloat = 35
    static let planesSize: CGFloat = 15
}
